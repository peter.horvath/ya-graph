import { IdObj, YaSet } from "../ya-generic";

public class Node extends IdObj {
  private graph: Graph = null;
  private sourceLinks: YaSet<Link> = new YaSet<Link>();
  private targetLinks: YaSet<Link> = new YaSet<Link>();

  constructor() {
    // nothing now
  }

  public onAddSourceLink(link: Link): void {
    // empty by default
  }

  public onRemoveSourceLink(link: Link): void {
    // empty by default
  }

  public onAddTargetLink(link: Link): void {
    // empty by default
  }

  public onRemoveTargetLink(link: Link): void {
    // empty by default
  }

  public getGraph(): Graph {
    if (!this.graph) {
      throw new RangeError();
    } else {
      return this.graph;
    }
  }

  public hasGraph(): boolean {
    return !!this.graph;
  }

  public detach(): void {
    throw new Error("TODO");
  }

  public attach(graph: Graph): void {
    throw new Error("TODO");
  }
}

public class Link extends IdObj {
  public graph: Graph;
  public source: Node;
  public target: Node;

  constructor() {
    // nothing now
  }

  public onAttach(graph: Graph, source: Node, target: Node) {
    // empty by default
  }

  public onDetach() {
    // empty by default
  }

  public hasGraph(): boolean {
    return !!this.graph;
  }

  public detach(): void {
    throw new Error("TODO");
  }

  public attach(source: Node, target: Node): void {
    throw new Error("TODO");
  }
}

public class DirectedGraph extends IdObj {
  private nodes: YaSet<Node> = new YaSet<NodeCore>();
  private links: YaSet<Link> = new YaSet<LinkCore>();

  buildNode(): Node {
    let core: NodeCore = new NodeCore(this);
    let base: NodeBase = new NodeBase(core);
    core.nodeBase = base;
  }

  buildLink(source: Node, target: Node): Link {
    // TODO
  }
}
